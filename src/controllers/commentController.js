const sequelize = require('../models/index');
const initModels = require('../models/init-models');
const models = initModels(sequelize);
const { successCode, failCode, errorCode } = require('../config/response');

// get comments by hinh_id
const getCommentsByImageId = async (req, res) => {
  try {
    const id = req.query.id;
    const comments = await models.binh_luan.findAll({
      where: {
        hinh_id: id,
      },
      include: [
        {
          model: models.nguoi_dung,
          attributes: [
            'nguoi_dung_id',
            'ho_ten',
            'email',
            'tuoi',
            'anh_dai_dien',
          ],
          as: 'nguoi_dung',
        },
      ],
    });
    if (comments.length > 0) successCode(res, comments, 'Thành công!');
    else failCode(res, comments, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// post comment
const postComment = async (req, res) => {
  try {
    const { hinh_id, nguoi_dung_id, noi_dung } = req.body;
    const currentDate = new Date();
    const options = { timezone: 'Asia/Ho_Chi_Minh' };
    const localDate = currentDate.toLocaleString('en-US', options);
    const comment = await models.binh_luan.create({
      hinh_id,
      nguoi_dung_id,
      noi_dung,
      ngay_binh_luan: localDate,
    });
    if (comment) successCode(res, comment, 'Thành công!');
    else failCode(res, comment, 'Không tạo được bình luận!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

module.exports = {
  getCommentsByImageId,
  postComment,
};
