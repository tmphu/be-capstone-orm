const sequelize = require('../models/index');
const initModels = require('../models/init-models');
const models = initModels(sequelize);
const { successCode, failCode, errorCode } = require('../config/response');
const { Op } = require('sequelize');

// get all images
const getAllImages = async (req, res) => {
  try {
    const images = await models.hinh_anh.findAll({
      include: [
        {
          model: models.nguoi_dung,
          attributes: ['nguoi_dung_id', 'ho_ten', 'email', 'tuoi'],
          as: 'nguoi_dung',
        },
      ],
    });
    if (images.length > 0) successCode(res, images, 'Thành công!');
    else failCode(res, images, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// get images by name
const getImagesByName = async (req, res) => {
  try {
    const { ten_hinh } = req.body;
    const images = await models.hinh_anh.findAll({
      where: {
        ten_hinh: {
          [Op.like]: `%${ten_hinh}%`,
        },
      },
      include: [
        {
          model: models.nguoi_dung,
          attributes: ['nguoi_dung_id', 'ho_ten', 'email', 'tuoi'],
          as: 'nguoi_dung',
        },
      ],
    });
    if (images.length > 0) successCode(res, images, 'Thành công!');
    else failCode(res, images, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// get image by id
const getImageById = async (req, res) => {
  try {
    const id = req.query.id;
    const image = await models.hinh_anh.findOne({
      where: {
        hinh_id: id,
      },
      include: [
        {
          model: models.nguoi_dung,
          attributes: ['nguoi_dung_id', 'ho_ten', 'email', 'tuoi'],
          as: 'nguoi_dung',
        },
      ],
    });
    if (image) successCode(res, image, 'Thành công!');
    else failCode(res, image, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// get thong tin da luu hinh nay chua theo id anh
const checkSavedImage = async (req, res) => {
  try {
    const { hinh_id, nguoi_dung_id } = req.body;
    const image = await models.luu_anh.findOne({
      where: {
        hinh_id: hinh_id,
        nguoi_dung_id: nguoi_dung_id,
      },
    });
    if (image) successCode(res, image, 'Đã lưu ảnh!');
    else failCode(res, image, 'Chưa lưu ảnh!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// get list of images saved by user id
const getImagesSavedByUserId = async (req, res) => {
  try {
    const { nguoi_dung_id } = req.body;
    const images = await models.luu_anh.findAll({
      where: {
        nguoi_dung_id: nguoi_dung_id,
      },
      include: [
        {
          model: models.hinh_anh,
          attributes: ['hinh_id', 'ten_hinh', 'mo_ta'],
          as: 'hinh_anh',
        },
      ],
    });
    if (images.length > 0) successCode(res, images, 'Thành công!');
    else failCode(res, images, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// get list of images created by user id
const getImagesCreatedByUserId = async (req, res) => {
  try {
    const { nguoi_dung_id } = req.body;
    const images = await models.hinh_anh.findAll({
      where: {
        nguoi_dung_id: nguoi_dung_id,
      },
    });
    if (images.length > 0) successCode(res, images, 'Thành công!');
    else failCode(res, images, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// post image
const postImage = async (req, res) => {
  try {
    const { ten_hinh, mo_ta, nguoi_dung_id, duong_dan } = req.body;
    const payload = {
      ten_hinh: ten_hinh,
      mo_ta: mo_ta,
      nguoi_dung_id: nguoi_dung_id,
      duong_dan: duong_dan,
    };
    const image = await models.hinh_anh.create(payload);
    if (image) successCode(res, image, 'Thành công!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// delete image by id
const deleteImageById = async (req, res) => {
  try {
    const id = req.query.id;
    const image = await models.hinh_anh.destroy({
      where: {
        hinh_id: id,
      },
    });
    if (image) successCode(res, image, 'Thành công!');
    else failCode(res, image, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

module.exports = {
  getAllImages,
  getImageById,
  getImagesByName,
  checkSavedImage,
  getImagesSavedByUserId,
  getImagesCreatedByUserId,
  postImage,
  deleteImageById,
};
