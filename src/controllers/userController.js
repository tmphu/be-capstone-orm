const sequelize = require('../models/index');
const initModels = require('../models/init-models');
const models = initModels(sequelize);
const { successCode, failCode, errorCode } = require('../config/response');
const bcrypt = require('bcrypt');
const { createToken } = require('../utils/jwtoken');

// signup
const signUp = async (req, res) => {
  try {
    const { email, password, name, age } = req.body;
    const payload = {
      email: email,
      mat_khau: bcrypt.hashSync(password, 10),
      ho_ten: name,
      tuoi: age,
    };
    const user = await models.nguoi_dung.create(payload);
    if (user) successCode(res, user, 'Đăng ký thành công');
  } catch (err) {
    console.log('server error: ', err);
  }
};

// login
const logIn = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await models.nguoi_dung.findOne({
      where: {
        email: email,
      },
    });
    if (user) {
      const isMatch = bcrypt.compareSync(password, user.mat_khau);
      if (isMatch) {
        const token = createToken(user.dataValues);
        successCode(res, token, 'Đăng nhập thành công');
      } else {
        failCode(res, user, 'Email và mật khẩu không trùng khớp');
      }
    } else {
      failCode(res, user, 'Không tìm thấy tài khoản');
    }
  } catch (err) {
    console.log('server error', err);
  }
};

// get user info
const getUserInfo = async (req, res) => {
  try {
    const { id } = req.query;
    const user = await models.nguoi_dung.findOne({
      where: {
        nguoi_dung_id: id,
      },
      attributes: ['nguoi_dung_id', 'ho_ten', 'email', 'tuoi', 'anh_dai_dien'],
    });
    if (user) successCode(res, user, 'Thành công!');
    else failCode(res, user, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

// edit user info
const editUserInfo = async (req, res) => {
  try {
    const { nguoi_dung_id, ho_ten, tuoi } = req.body;
    const user = await models.nguoi_dung.update(
      {
        ho_ten: ho_ten,
        tuoi: tuoi,
      },
      {
        where: {
          nguoi_dung_id: nguoi_dung_id,
        },
      }
    );
    if (user) successCode(res, user, 'Thành công!');
    else failCode(res, user, 'Không tìm thấy dữ liệu!');
  } catch (err) {
    console.log('server error: ', err);
    errorCode(res, err);
  }
};

module.exports = {
  signUp,
  logIn,
  getUserInfo,
  editUserInfo,
};
