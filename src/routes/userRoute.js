'use strict';
const express = require('express');
const userRoute = express.Router();
const {
  logIn,
  signUp,
  getUserInfo,
  editUserInfo,
} = require('../controllers/userController');
const { verifyToken } = require('../utils/jwtoken');

// login
userRoute.post('/login', logIn);

// signup
userRoute.post('/signup', signUp);

// get user info
userRoute.get('/getuserinfo', verifyToken, getUserInfo);

// edit user info
userRoute.put('/edit', verifyToken, editUserInfo);

module.exports = userRoute;
