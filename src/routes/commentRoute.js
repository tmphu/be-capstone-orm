'use strict';
const express = require('express');
const commentRoute = express.Router();
const {
  getCommentsByImageId,
  postComment,
} = require('../controllers/commentController');
const { verifyToken } = require('../utils/jwtoken');

// get comments by hinh_id
commentRoute.get('/getcommentsbyimage', verifyToken, getCommentsByImageId);

// post comment
commentRoute.post('/post', verifyToken, postComment);

module.exports = commentRoute;
