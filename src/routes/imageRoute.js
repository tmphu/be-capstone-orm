'use strict';
const express = require('express');
const imageRoute = express.Router();
const {
  getAllImages,
  getImageById,
  postImage,
  getImagesByName,
  checkSavedImage,
  getImagesSavedByUserId,
  getImagesCreatedByUserId,
  deleteImageById,
} = require('../controllers/imageController');
const { verifyToken } = require('../utils/jwtoken');

// get all images
imageRoute.get('/', verifyToken, getAllImages);

// get images by name
imageRoute.get('/searchbyname', verifyToken, getImagesByName);

// get image by id
imageRoute.get('/searchbyid', verifyToken, getImageById);

// get thong tin da luu hinh nay chua theo id anh
imageRoute.get('/checksavedimage', verifyToken, checkSavedImage);

// get list of images saved by user id
imageRoute.get('/getimagessavedbyuserid', verifyToken, getImagesSavedByUserId);

// get list of images created by user id
imageRoute.get(
  '/getimagescreatedbyuserid',
  verifyToken,
  getImagesCreatedByUserId
);

// delete image by id
imageRoute.delete('/deletebyid', verifyToken, deleteImageById);

// post image
imageRoute.post('/post', verifyToken, postImage);

module.exports = imageRoute;
