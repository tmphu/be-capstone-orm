const fs = require('fs');
const express = require('express');
const cors = require('cors');
const app = express();
const port = 8080;

app.use(cors());
app.use(express.json());
app.use(express.static('.'));

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`Capstone ORM listening on port ${port}!`));

const rootRoute = require('./routes/rootRoute');
app.use('/api', rootRoute);
