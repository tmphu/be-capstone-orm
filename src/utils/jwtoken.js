const jwt = require('jsonwebtoken');

const createToken = (payload) => {
  return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '10m' });
};

const verifyToken = (req, res, next) => {
  try {
    const { token } = req.headers;
    const isMatch = jwt.verify(token, process.env.JWT_SECRET);
    if (isMatch) {
      next();
    }
  } catch (error) {
    res.status(401).send(error.message);
  }
};

module.exports = { createToken, verifyToken };
